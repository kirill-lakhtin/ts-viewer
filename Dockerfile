FROM openjdk:8
RUN mkdir /var/buildviewer
COPY ./target/buildviewer.jar /var/buildviewer
WORKDIR /var/buildviewer
EXPOSE 8080
ENTRYPOINT ["java","-jar","buildviewer.jar"]