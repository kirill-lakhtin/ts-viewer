package com.jetbrains.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * A model that represents a TC host.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "host")
public class Host {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hostGenerator")
  @SequenceGenerator(name = "hostGenerator", sequenceName = "host_sequence")
  private Long id;

  @Column(name = "url")
  private String url;

  @OneToMany(mappedBy = "host")
  private List<Build> builds;

  public Host(Long id, String url) {
    this.id = id;
    this.url = url;
  }
}
