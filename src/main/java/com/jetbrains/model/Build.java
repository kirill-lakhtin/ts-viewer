package com.jetbrains.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;
import java.util.Comparator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * A model that represents a single build in the TS.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "build")
public class Build {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "buildGenerator")
  @SequenceGenerator(name = "buildGenerator", sequenceName = "build_sequence")
  private long id;

  @ManyToOne
  @JoinColumn(name = "host")
  private Host host;

  @Column(name = "start_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd'T'HHmmssZ")
  private Calendar startDate;

  @Column(name = "build_type_id")
  private String buildTypeId;

  /**
   * Comparator for comparing builds ignoring id.
   */
  public static Comparator<Build> getComparatorWithoutId() {
    return Comparator.comparing(Build::getBuildTypeId)
        .thenComparing(b -> b.getStartDate())
        .thenComparing(b -> b.getHost().getId());
  }
}
