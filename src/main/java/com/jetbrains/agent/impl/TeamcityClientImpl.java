package com.jetbrains.agent.impl;

import com.jetbrains.agent.TeamcityClient;
import com.jetbrains.model.Build;
import com.jetbrains.model.Host;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.TimeZone;
import javax.annotation.Nonnull;

/**
 * {@link TeamcityClient} implementation.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Component
@Slf4j
public class TeamcityClientImpl implements TeamcityClient {

  private static final String RUNNING_BUILD_QUERY = "/guestAuth/app/rest/builds?locator"
      + "=state:running&fields=build(buildTypeId,startDate)";
  private final RestTemplate restTemplate;

  public TeamcityClientImpl(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Override
  public Collection<Build> getAllRunningBuilds(final @Nonnull Host host) {
    String url = host.getUrl() + RUNNING_BUILD_QUERY;
    ResponseEntity<TeamCityBuildDto> response = restTemplate
        .getForEntity(url, TeamCityBuildDto.class);
    Collection<Build> builds = response.getBody().getBuild();
    builds = Optional.ofNullable(builds).orElse(Collections.emptyList());
    builds.forEach(build -> {
      build.setHost(host);
      //set default timezone, TC instances can have a different one
      build.getStartDate().setTimeZone(TimeZone.getDefault());
    });
    return builds;
  }
}
