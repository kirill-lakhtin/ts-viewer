package com.jetbrains.agent.impl;

import com.jetbrains.model.Build;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Data transfer object that represent a TC response from 'builds' resource.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TeamCityBuildDto {
  private List<Build> build;
}
