package com.jetbrains.agent;

import com.jetbrains.model.Build;
import com.jetbrains.model.Host;

import java.util.Collection;

/**
 * Very simple client for TS API. Looks like reinventing the wheel, but JB-provided client can't
 * query builds by state.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public interface TeamcityClient {

  /**
   * Returns all running builds in the specified host.
   *
   * @throws org.springframework.web.client.RestClientException if TC replies with error.
   */
  Collection<Build> getAllRunningBuilds(Host host);
}
