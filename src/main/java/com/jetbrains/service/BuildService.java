package com.jetbrains.service;

import com.jetbrains.model.Build;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * A service responsible for operations on {@link com.jetbrains.model.Build}.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public interface BuildService {

  /**
   * Returns a page of builds by the specified page request.
   */
  Page<Build> getBuilds(Pageable pageRequest);
}
