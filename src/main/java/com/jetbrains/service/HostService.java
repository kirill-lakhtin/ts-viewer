package com.jetbrains.service;

import com.jetbrains.model.Host;
import com.jetbrains.service.exception.HostAlreadyExistsException;
import com.jetbrains.service.exception.HostDoesNotExistException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nonnull;

/**
 * A service responsible for operations on {@link com.jetbrains.model.Host}.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public interface HostService {

  /**
   * Creates a new host.
   *
   * @param url the url of the new host.
   * @return a {@link Host} object that represents a newly created host.
   * @throws HostAlreadyExistsException if a host with the specified url already exists.
   */
  @Nonnull
  Host add(@Nonnull String url) throws HostAlreadyExistsException;

  /**
   * Deletes an existing host.
   *
   * @param id id of the host that should be deleted.
   * @return the removed host.
   */
  Host delete(long id) throws HostDoesNotExistException;

  /**
   * Returns a page of hosts by page request.
   *
   * @param pageable an object that specifies a page num, page size and sorting direction.
   * @return page of hosts.
   */
  Page<Host> getHosts(Pageable pageable);
}
