package com.jetbrains.service.impl;

import com.jetbrains.agent.TeamcityClient;
import com.jetbrains.dao.BuildRepository;
import com.jetbrains.dao.HostRepository;
import com.jetbrains.model.Build;
import com.jetbrains.model.Host;
import com.jetbrains.service.WebSocketNotifier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Background-running component that collects all running builds and stores them in the
 * build table.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Component
@Slf4j
public class BuildCollector {

  private static final String UPDATE_LOCK_QUERY = "update lock set time = ? "
      + "where time < ? or time is null";

  private int period;
  private int clientTimeout;

  private final JdbcTemplate jdbcTemplate;
  private final TeamcityClient teamcityClient;
  private final HostRepository hostRepository;
  private final BuildRepository buildRepository;
  private final ExecutorService executorService;
  private final TransactionTemplate transactionTemplate;
  private final WebSocketNotifier webSocketNotifier;

  /**
   * Creates a build collector.
   */
  public BuildCollector(JdbcTemplate jdbcTemplate, TeamcityClient teamcityClient,
      HostRepository hostRepository, ExecutorService executorService,
      TransactionTemplate transactionTemplate, BuildRepository buildRepository,
      WebSocketNotifier webSocketNotifier,
      @Value("${client.timeout}") int clientTimeout,
      @Value("${buildCollector.period}") int period) {
    this.jdbcTemplate = jdbcTemplate;
    this.teamcityClient = teamcityClient;
    this.hostRepository = hostRepository;
    this.executorService = executorService;
    this.transactionTemplate = transactionTemplate;
    this.buildRepository = buildRepository;
    this.clientTimeout = clientTimeout;
    this.period = period;
    this.webSocketNotifier = webSocketNotifier;
  }

  /**
   * Scheduled method that will collect builds from TS and store them in the DB.
   */
  @Scheduled(fixedDelayString = "${buildCollector.period}")
  public void collectBuilds() {
    //try to acquired db-based lock to exclusively ask TC instances
    boolean lockAcquired = transactionTemplate.execute(transactionStatus -> tryIncrementLock());

    //if lock acquired - truncate table and write new running builds
    if (lockAcquired) {
      Iterable<Host> hosts = hostRepository.findAll();
      List<Build> builds = collectAllBuilds(hosts);
      Boolean needToUpdate = transactionTemplate.execute(transactionStatus -> {
        Set<Build> collectedBuilds = new TreeSet<>(Build.getComparatorWithoutId());
        collectedBuilds.addAll(builds);
        Iterable<Build> oldBuilds = buildRepository.findAll();

        List<Build> buildToSave = new ArrayList<>();
        List<Build> buildToDelete = new ArrayList<>();
        for (Build build : oldBuilds) {
          if (!collectedBuilds.remove(build)) {
            buildToDelete.add(build);
          }
        }
        if (!collectedBuilds.isEmpty()) {
          buildToSave.addAll(collectedBuilds);
        }

        if (!buildToDelete.isEmpty()) {
          buildRepository.delete(buildToDelete);
        }

        if (!buildToSave.isEmpty()) {
          buildRepository.save(buildToSave);
        }
        return !buildToSave.isEmpty() || !buildToDelete.isEmpty();
      });

      if (needToUpdate) {
        //send notification if there are any changes in the db
        webSocketNotifier.sendBuildNotification();
      }
    }
  }

  /**
   * Collects all builds from specified hosts.
   */
  private List<Build> collectAllBuilds(Iterable<Host> hosts) {
    Collection<Future<Collection<Build>>> futures = new ArrayList<>();
    for (Host host : hosts) {
      futures.add(executorService.submit(() -> getAllRunningBuilds(host)));
    }

    List<Build> builds = new ArrayList<>();
    for (Future<Collection<Build>> future : futures) {
      try {
        builds.addAll(future.get(clientTimeout, TimeUnit.SECONDS));
      } catch (InterruptedException | ExecutionException | TimeoutException exception) {
        //skip errors, these hosts simply won't be included in the result list
        log.error("An error occured", exception);
      }
    }
    return builds;
  }

  /**
   * Returns all running builds from TC or empty collection if something went wrong during request.
   */
  private Collection<Build> getAllRunningBuilds(Host host) {
    try {
      return teamcityClient.getAllRunningBuilds(host);
    } catch (RestClientException exception) {
      log.error("Error during s remote call to host " + host.getUrl(), exception);
      return Collections.emptyList();
    }
  }

  /**
   * Tries to acquire db-based lock.
   *
   * @return true is the lock is acquired, false otherwise.
   */
  private boolean tryIncrementLock() {
    Calendar now = Calendar.getInstance();
    Calendar previosTime = Calendar.getInstance();
    previosTime.setTimeInMillis(now.getTimeInMillis() - period);
    //use plain old query to totally assure that there will be no 'lost updates'
    int updated = jdbcTemplate.update(UPDATE_LOCK_QUERY, now, previosTime);
    return updated > 0;
  }
}
