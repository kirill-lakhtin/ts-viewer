package com.jetbrains.service.impl;

import com.jetbrains.dao.HostRepository;
import com.jetbrains.model.Host;
import com.jetbrains.service.HostService;
import com.jetbrains.service.exception.HostAlreadyExistsException;
import com.jetbrains.service.exception.HostDoesNotExistException;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;

/**
 * {@link HostService} implementation.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Service
@Transactional
public class HostServiceImpl implements HostService {

  private final HostRepository hostRepository;

  @Autowired
  public HostServiceImpl(HostRepository hostRepository) {
    this.hostRepository = hostRepository;
  }

  @Override
  public Host add(@Nonnull final String url) throws HostAlreadyExistsException {
    if (hostRepository.findByUrl(url) == null) {
      final Host newHost = new Host();
      newHost.setUrl(url);
      return hostRepository.save(newHost);
    } else {
      throw new HostAlreadyExistsException(url);
    }
  }

  @Override
  public Host delete(final long id) throws HostDoesNotExistException {
    Host host = hostRepository.findOne(id);
    if (host != null) {
      hostRepository.delete(id);
      // initialize lazy-loaded collection, not a performance hit for a single object.
      Hibernate.initialize(host.getBuilds());
      return host;
    } else {
      throw new HostDoesNotExistException(id);
    }
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Host> getHosts(@Nonnull final Pageable pageable) {
    return hostRepository.findAll(pageable);
  }
}
