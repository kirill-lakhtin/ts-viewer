package com.jetbrains.service.impl;

import com.jetbrains.dao.BuildRepository;
import com.jetbrains.model.Build;
import com.jetbrains.service.BuildService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;

/**
 * {@link BuildService} implementation.

 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Service
@Transactional
public class BuildServiceImpl implements BuildService {

  private final BuildRepository buildRepository;

  public BuildServiceImpl(BuildRepository buildRepository) {
    this.buildRepository = buildRepository;
  }

  @Override
  public Page<Build> getBuilds(@Nonnull final Pageable pageable) {
    return buildRepository.findAll(pageable);
  }
}
