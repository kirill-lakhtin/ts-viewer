package com.jetbrains.service.impl;

import com.jetbrains.service.WebSocketNotifier;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class WebSocketNotifierImpl implements WebSocketNotifier {

  private SimpMessagingTemplate template;

  public WebSocketNotifierImpl(SimpMessagingTemplate template) {
    this.template = template;
  }

  @Override
  public void sendHostNotification() {
    template.convertAndSend("/topic/host", "refresh");
  }

  @Override
  public void sendBuildNotification() {
    template.convertAndSend("/topic/build", "refresh");
  }
}
