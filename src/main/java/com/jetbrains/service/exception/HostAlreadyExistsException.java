package com.jetbrains.service.exception;

/**
 * Exception that tells that a host with the specified url has been already added.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public class HostAlreadyExistsException extends BuildViewerException {

  private final String url;

  public HostAlreadyExistsException(String url) {
    this.url = url;
  }

  @Override
  public String getMessage() {
    return String.format("Host with url = %s already exists", url);
  }
}
