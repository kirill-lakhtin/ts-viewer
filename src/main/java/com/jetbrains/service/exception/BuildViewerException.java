package com.jetbrains.service.exception;

/**
 * Base class for all exceptions in the application.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public abstract class BuildViewerException extends Exception {

}
