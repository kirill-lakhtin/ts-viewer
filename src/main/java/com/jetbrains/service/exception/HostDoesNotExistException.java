package com.jetbrains.service.exception;

/**
 * Exception that tells that host with the specified id doesn't exists.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public class HostDoesNotExistException extends BuildViewerException {

  private final long id;

  public HostDoesNotExistException(long id) {
    this.id = id;
  }

  @Override
  public String getMessage() {
    return String.format("A host with id = %d doesn't exist", id);
  }
}
