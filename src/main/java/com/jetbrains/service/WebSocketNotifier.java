package com.jetbrains.service;

/**
 * Sends notifications to WS clients.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public interface WebSocketNotifier {

  /**
   * Sends a notification which tells to clients that host list is changed.
   */
  void sendHostNotification();

  /**
   * Sends a notification which tells to clients that build list is changed.
   */
  void sendBuildNotification();
}
