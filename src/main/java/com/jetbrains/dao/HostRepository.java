package com.jetbrains.dao;

import com.jetbrains.model.Host;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository for performing db operations on {@link Host}.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public interface HostRepository extends PagingAndSortingRepository<Host, Long> {

  Host findByUrl(String url);
}
