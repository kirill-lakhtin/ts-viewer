package com.jetbrains.dao;

import com.jetbrains.model.Build;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository for performing db operations on {@link Build}.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public interface BuildRepository extends PagingAndSortingRepository<Build, Long> {

}
