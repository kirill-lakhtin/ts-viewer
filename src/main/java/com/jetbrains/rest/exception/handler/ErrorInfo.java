package com.jetbrains.rest.exception.handler;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Simple object for returning a error code from REST API.
 * Do not serialize null values.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Data
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ErrorInfo {

  private String errorCode;
}
