package com.jetbrains.rest.exception.handler;

import com.jetbrains.rest.exception.BadRequestException;
import com.jetbrains.rest.exception.ConflictException;
import com.jetbrains.rest.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * App's exception handler.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Slf4j
@ControllerAdvice
public class WebExceptionHandler {

  @ExceptionHandler(BadRequestException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorInfo badRequest(BadRequestException exception) {
    return new ErrorInfo(exception.getErrorCode());
  }

  @ExceptionHandler(ConflictException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.CONFLICT)
  public ErrorInfo conflict(ConflictException exception) {
    return new ErrorInfo(exception.getErrorCode());
  }

  @ExceptionHandler(NotFoundException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ErrorInfo notFound(NotFoundException exception) {
    return new ErrorInfo(exception.getErrorCode());
  }

  @ExceptionHandler(Exception.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public void fallback(Exception exception) {
    log.error("Error", exception);
  }
}
