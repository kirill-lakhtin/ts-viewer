package com.jetbrains.rest.exception;

/**
 * A base class for all web exceptions.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public abstract class WebException extends Exception {

  private String errorCode;

  public WebException() {
  }

  public WebException(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorCode() {
    return errorCode;
  }
}
