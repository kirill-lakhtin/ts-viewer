package com.jetbrains.rest.exception;

/**
 * Represents a http response with the code 400.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public class BadRequestException extends WebException {

  public BadRequestException(String errorCode) {
    super(errorCode);
  }
}
