package com.jetbrains.rest.exception;

/**
 * Represents a http response with the code 404.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public class NotFoundException extends WebException {

  public NotFoundException() {
  }
}
