package com.jetbrains.rest.exception;

/**
 * Represents a http response with the code 409.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
public class ConflictException extends WebException {

  public ConflictException(String errorCode) {
    super(errorCode);
  }
}
