package com.jetbrains.rest.dto;

import com.jetbrains.model.Host;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data transfer object for host.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HostDto {

  public HostDto(Host host) {
    this.id = host.getId();
    this.url = host.getUrl();
  }

  private Long id;
  private String url;
}
