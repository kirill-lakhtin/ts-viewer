package com.jetbrains.rest.dto;

import com.jetbrains.model.Build;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;

/**
 * Data transfer object for a 'build' model.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BuildDto {

  /**
   * Constructs dto from {@link Build}.
   */
  public BuildDto(Build build) {
    this.host = build.getHost().getUrl();
    this.startDate = build.getStartDate();
    this.buildTypeId = build.getBuildTypeId();
  }

  private String host;
  private Calendar startDate;
  private String buildTypeId;
}
