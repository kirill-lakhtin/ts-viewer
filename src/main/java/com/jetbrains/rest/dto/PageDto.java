package com.jetbrains.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Data transfer object for a page of elements.
 * @param <T> elements type.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageDto<T> {

  public PageDto(Page<T> page) {
    this.totalCount = page.getTotalElements();
    this.elements = page.getContent();
  }

  private long totalCount;
  private List<T> elements;
}
