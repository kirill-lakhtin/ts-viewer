package com.jetbrains.rest;

import com.jetbrains.model.Build;
import com.jetbrains.rest.dto.BuildDto;
import com.jetbrains.rest.dto.PageDto;
import com.jetbrains.rest.exception.BadRequestException;
import com.jetbrains.service.BuildService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for the '/build' resource.
 */
@RestController
@RequestMapping("/build")
public class BuildController {

  private final BuildService buildService;

  public BuildController(final BuildService buildService) {
    this.buildService = buildService;
  }

  /**
   * Returns a page of builds, supports paging and sorting.
   *
   * @param page page number (0-based). Default - 0.
   * @param pageSize page size. Default - 10.
   * @param sortField the field for sorting. Default - there are no any sorting guarantees.
   * @param descSort sorting direction. True for desc and false for asc.
   */
  @RequestMapping
  public PageDto<BuildDto> getBuilds(
      @RequestParam(required = false, defaultValue = "0") final int page,
      @RequestParam(required = false, defaultValue = "10") final int pageSize,
      @RequestParam(required = false) final String sortField,
      @RequestParam(required = false, defaultValue = "false") final boolean descSort)
      throws BadRequestException {

    if (page < 0) {
      throw new BadRequestException("page");
    }
    if (pageSize < 0) {
      throw new BadRequestException("pageSize");
    }

    PageRequest pageRequest;
    if (sortField != null) {
      Direction direction = descSort ? Direction.DESC : Direction.ASC;
      pageRequest = new PageRequest(page, pageSize, direction, sortField);
    } else {
      pageRequest = new PageRequest(page, pageSize);
    }
    Page<Build> buildsPage = buildService.getBuilds(pageRequest);
    Page<BuildDto> convertedPage = buildsPage.map(BuildDto::new);
    return new PageDto<>(convertedPage);
  }
}
