package com.jetbrains.rest;

import com.jetbrains.model.Host;
import com.jetbrains.rest.dto.HostDto;
import com.jetbrains.rest.dto.PageDto;
import com.jetbrains.rest.exception.BadRequestException;
import com.jetbrains.rest.exception.ConflictException;
import com.jetbrains.rest.exception.NotFoundException;
import com.jetbrains.service.HostService;
import com.jetbrains.service.WebSocketNotifier;
import com.jetbrains.service.exception.HostAlreadyExistsException;
import com.jetbrains.service.exception.HostDoesNotExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for the '/host' resource.
 */
@RestController
@RequestMapping("/host")
public class HostController {

  private final HostService hostService;
  private final WebSocketNotifier webSocketNotifier;

  @Autowired
  public HostController(HostService hostService, WebSocketNotifier webSocketNotifier) {
    this.hostService = hostService;
    this.webSocketNotifier = webSocketNotifier;
  }

  /**
   * Creates a new host and returns back an entity with id.
   *
   * @param dto describes a new TC host.
   * @return object that contains an id of the created host.
   */
  @RequestMapping(method = RequestMethod.POST)
  public HostDto create(@RequestBody HostDto dto) throws ConflictException {
    try {
      Host newHost = hostService.add(dto.getUrl());
      webSocketNotifier.sendHostNotification();
      return new HostDto(newHost);
    } catch (HostAlreadyExistsException exception) {
      throw new ConflictException("url");
    }
  }

  /**
   * Deletes an existing host.
   *
   * @param id host's id.
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public void delete(@PathVariable long id) throws NotFoundException {
    try {
      Host removedHost = hostService.delete(id);
      if (!CollectionUtils.isEmpty(removedHost.getBuilds())) {
        webSocketNotifier.sendBuildNotification();
      }
      webSocketNotifier.sendHostNotification();
    } catch (HostDoesNotExistException exception) {
      throw new NotFoundException();
    }
  }

  /**
   * Returns a page of hosts.
   *
   * @param page page number (0-based). Default - 0.
   * @param pageSize page size. Default - 10.
   * @return a page with hosts collected using specified parameters.
   */
  @RequestMapping
  public PageDto<HostDto> get(@RequestParam(required = false, defaultValue = "0") Integer page,
      @RequestParam(required = false, defaultValue = "10") Integer pageSize)
      throws BadRequestException {
    //validate incoming params
    if (page < 0) {
      throw new BadRequestException("page");
    }
    if (pageSize < 1) {
      throw new BadRequestException("pageSize");
    }

    Page<Host> hostsPage = hostService.getHosts(new PageRequest(page, pageSize));
    Page<HostDto> convertedPage = hostsPage.map(HostDto::new);
    return new PageDto<>(convertedPage);
  }
}
