package com.jetbrains.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Scheduling config.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@ConditionalOnProperty(value = "scheduling.enable", havingValue = "true", matchIfMissing = true)
@Configuration
@EnableScheduling
public class SchedulingConfiguration {

}
