package com.jetbrains.rest;

 import com.jetbrains.model.Build;
import com.jetbrains.model.Host;
import com.jetbrains.rest.dto.HostDto;
import com.jetbrains.rest.dto.PageDto;
import com.jetbrains.rest.exception.BadRequestException;
import com.jetbrains.rest.exception.ConflictException;
import com.jetbrains.rest.exception.NotFoundException;
import com.jetbrains.service.HostService;
import com.jetbrains.service.WebSocketNotifier;
import com.jetbrains.service.exception.HostAlreadyExistsException;
import com.jetbrains.service.exception.HostDoesNotExistException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Collections;

/**
 * Tests for {@link HostController}.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@RunWith(MockitoJUnitRunner.class)
public class HostControllerTest {

  private static final String TEST_URL = "http://test.com";
  private HostController controller;

  @Mock
  private HostService hostService;

  @Mock
  private WebSocketNotifier webSocketNotifier;

  @Before
  public void setUp() throws Exception {
    controller = new HostController(hostService, webSocketNotifier);
  }

  /**
   * Checks that underlying service is called with the correct arguments, the
   * result is properly converted and ws notification is sent.
   */
  @Test
  public void createOK() throws Exception {
    Mockito.when(hostService.add(TEST_URL)).thenReturn(new Host(1L, TEST_URL));
    HostDto dto = new HostDto();
    dto.setUrl(TEST_URL);

    HostDto result = controller.create(dto);

    Mockito.verify(webSocketNotifier).sendHostNotification();
    Assert.assertEquals(TEST_URL, result.getUrl());
    Assert.assertEquals(1L, (long)result.getId());
  }

  /**
   * Checks that if underlying service throws an exception, it will be translated to the correct
   * web exception and ws notification won't be sent.
   */
  @Test(expected = ConflictException.class)
  public void createAlreadyExists() throws Exception {
    Mockito.when(hostService.add(TEST_URL)).thenThrow(new HostAlreadyExistsException(TEST_URL));
    HostDto dto = new HostDto();
    dto.setUrl(TEST_URL);

    controller.create(dto);

    Mockito.verify(webSocketNotifier, Mockito.never()).sendHostNotification();
  }

  /**
   *  Checks that underlying service is called with the correct arguments and ws notification is
   *  sent.
   */
  @Test
  public void deleteOK() throws Exception {
    Host host = new Host();
    Mockito.when(hostService.delete(1L)).thenReturn(host);

    controller.delete(1L);

    Mockito.verify(webSocketNotifier).sendHostNotification();
    Mockito.verify(webSocketNotifier, Mockito.never()).sendBuildNotification();
  }

  /**
   *  Checks that underlying service is called with the correct arguments and, if the removed
   *  host has builds, two types of ws notifications are thrown - one for host and one for builds.
   */
  @Test
  public void deleteHostWithBuilds() throws Exception {
    Host host = new Host();
    host.setBuilds(Collections.singletonList(new Build()));
    Mockito.when(hostService.delete(1L)).thenReturn(host);

    controller.delete(1L);

    Mockito.verify(webSocketNotifier).sendHostNotification();
    Mockito.verify(webSocketNotifier).sendBuildNotification();
  }

  /**
   * Checks that if underlying service throws an exception, it will be translated to the correct
   * web exception and ws notification won't be sent.
   */
  @Test(expected = NotFoundException.class)
  public void deleteDoesNotExist() throws Exception {
    Mockito.doThrow(new HostDoesNotExistException(1L)).when(hostService).delete(1L);

    controller.delete(1L);

    Mockito.verify(webSocketNotifier, Mockito.never()).sendHostNotification();
  }

  /**
   * Checks that underlying service is called with the correct arguments and the
   * result is properly converted.
   */
  @Test
  public void getOK() throws Exception {
    ArgumentCaptor<PageRequest> captor = ArgumentCaptor
        .forClass(PageRequest.class);

    Page<Host> page = new PageImpl<>(Collections.singletonList(new Host(1L, TEST_URL)), null, 10);
    Mockito.when(hostService.getHosts(captor.capture())).thenReturn(page);

    PageDto<HostDto> dto = controller.get(0, 10);

    Assert.assertEquals(0, captor.getValue().getPageNumber());
    Assert.assertEquals(10, captor.getValue().getPageSize());
    Assert.assertEquals(10, dto.getTotalCount());
    Assert.assertEquals(1L, (long)dto.getElements().get(0).getId());
    Assert.assertEquals(TEST_URL, dto.getElements().get(0).getUrl());
  }

  /**
   * Checks that the controller throws {@link BadRequestException} if page is less than 0.
   */
  @Test(expected = BadRequestException.class)
  public void getInvalidPage() throws Exception {
    controller.get(-10, 10);
  }

  /**
   * Checks that the controller throws {@link BadRequestException} if pageSize is less than 0.
   */
  @Test(expected = BadRequestException.class)
  public void getInvalidPageSize() throws Exception {
    controller.get(0, -10);
  }

}