package com.jetbrains.rest;

import com.jetbrains.model.Build;
import com.jetbrains.model.Host;
import com.jetbrains.rest.dto.BuildDto;
import com.jetbrains.rest.dto.PageDto;
import com.jetbrains.rest.exception.BadRequestException;
import com.jetbrains.service.BuildService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Calendar;
import java.util.Collections;

/**
 * Tests for {@link BuildController}.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@RunWith(MockitoJUnitRunner.class)
public class BuildControllerTest {

  private static final String TEST_URL = "http://test.com";
  private static final String BUILD_TYPE_ID = "build";
  private BuildController controller;

  @Mock
  private BuildService buildService;

  @Before
  public void setUp() throws Exception {
    controller = new BuildController(buildService);
  }

  /**
   * Checks that BadRequestException will be thrown if page is less than 0.
   */
  @Test(expected = BadRequestException.class)
  public void getBuildsInvalidPage() throws Exception {
    controller.getBuilds(-10, 10, null, false);
  }

  /**
   * Checks that BadRequestException will be thrown if pageSize is less than 0.
   */
  @Test(expected = BadRequestException.class)
  public void getBuildsInvalidPageSize() throws Exception {
    controller.getBuilds(0, -10, null, false);
  }

  /**
   * Checks that underlying service called with correct arguments in case of DESC sorting
   * and the result id converted correctly.
   */
  @Test
  public void getBuildsDescSorting() throws Exception {
    Calendar calendar = Calendar.getInstance();
    Build build = new Build(1L, new Host(1L, TEST_URL), calendar, BUILD_TYPE_ID);
    Page<Build> page = new PageImpl<>(Collections.singletonList(build), null, 10);
    ArgumentCaptor<PageRequest> captor = ArgumentCaptor.forClass(PageRequest.class);
    Mockito.when(buildService.getBuilds(captor.capture())).thenReturn(page);

    PageDto<BuildDto> result = controller.getBuilds(0, 20, "host", true);

    //verify translation web params to page request
    PageRequest pageRequest = captor.getValue();
    Assert.assertEquals(0, pageRequest.getPageNumber());
    Assert.assertEquals(20, pageRequest.getPageSize());
    Assert.assertTrue(pageRequest.getSort().getOrderFor("host").getDirection().isDescending());

    //verifying result conversion
    Assert.assertEquals(10, result.getTotalCount());
    BuildDto buildDto = result.getElements().get(0);
    Assert.assertEquals(TEST_URL, buildDto.getHost());
    Assert.assertEquals(calendar, buildDto.getStartDate());
    Assert.assertEquals(BUILD_TYPE_ID, buildDto.getBuildTypeId());
  }

  /**
   * Checks that underlying service called with correct arguments in case of not specified sorting.
   */
  @Test
  public void getBuildsWithoutSorting() throws Exception {
    Calendar instance = Calendar.getInstance();
    instance.setTimeInMillis(100);
    Build build = new Build(1L, new Host(1L, TEST_URL), instance, BUILD_TYPE_ID);
    Page<Build> page = new PageImpl<>(Collections.singletonList(build), null, 10);
    ArgumentCaptor<PageRequest> captor = ArgumentCaptor.forClass(PageRequest.class);
    Mockito.when(buildService.getBuilds(captor.capture())).thenReturn(page);

    PageDto<BuildDto> result = controller.getBuilds(0, 20, null, false);

    //verify translation web params to page request
    PageRequest pageRequest = captor.getValue();
    Assert.assertEquals(0, pageRequest.getPageNumber());
    Assert.assertEquals(20, pageRequest.getPageSize());

    //verifying result conversion
    Assert.assertEquals(10, result.getTotalCount());
    BuildDto buildDto = result.getElements().get(0);
    Assert.assertEquals(TEST_URL, buildDto.getHost());
    Assert.assertEquals(instance, buildDto.getStartDate());
    Assert.assertEquals(BUILD_TYPE_ID, buildDto.getBuildTypeId());
  }
}