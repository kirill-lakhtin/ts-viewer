package com.jetbrains.integration;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jetbrains.agent.TeamcityClient;
import com.jetbrains.configuration.Application;
import com.jetbrains.dao.HostRepository;
import com.jetbrains.model.Build;
import com.jetbrains.model.Host;
import com.jetbrains.service.BuildService;
import com.jetbrains.service.WebSocketNotifier;
import com.jetbrains.service.impl.BuildCollector;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Integration test for testing concurrent reading and writing builds.
 * This test covers two issues:
 *
 * 1) Reader reads an empty page. This means the dirty read, where the reader operation take place
 * between removing old builds and saving new ones. In properly organized transactions this issue
 * shouldn't occur.
 *
 * 2) Reader reads a page with records from different generations. This issue can take place if
 * the writer firstly write the new data and then deletes old builds. In properly organized
 * transactions this issue shouldn't occur.
 *
 * WARN: The test can take some time.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = Application.class)
@TestExecutionListeners({ MockitoTestExecutionListener.class,DependencyInjectionTestExecutionListener.class,
    TransactionDbUnitTestExecutionListener.class})
public class ConcurrentReadAndWriteBuildTest {

  /**
   * Number of test iterations (de-facto, the number of generations)
   */
  private static final int ITERATIONS_NUMBER = 1000;

  /**
   * How much elements will be generated for an iteration.
   */
  private static final int ITERATION_SIZE = 30;

  /**
   * How much elements will be queried by reader.
   */
  private static final int PAGE_SIZE = 10;

  @Autowired
  private BuildCollector collector;

  @Autowired
  private BuildService buildService;

  @Autowired
  private HostRepository hostRepository;

  @MockBean
  private WebSocketNotifier webSocketNotifier;

  @MockBean
  private TeamcityClient teamcityClient;

  private Random random = new Random();
  private AtomicInteger iterationCounter  = new AtomicInteger(0);
  private Calendar instance = Calendar.getInstance();

  //separate executors for read / write operations
  private ExecutorService writingExecutor = Executors.newSingleThreadExecutor();
  private ExecutorService readingExecutor = Executors.newSingleThreadExecutor();

  @Test
  @DirtiesContext
  @DatabaseSetup("classpath:/before.xml")
  public void concurrentTest() throws Exception {

    Host host = hostRepository.findOne(1L);

    //initial data, used if the reading thread starts firstly
    runCollectionGeneration(host);

    /*
      Concurrent writing. ITERATION_NUMBER times creates a new generation of data and runs
      BuildCollector, which lead to the new generation is written in the DB.
    */
    CompletableFuture<Void> writingFuture = CompletableFuture.runAsync(() -> {
      for (int i = 0; i < ITERATIONS_NUMBER; i++) {
        runCollectionGeneration(host);
      }
    }, writingExecutor);

    /*
      Concurrent reading. Constantly reads a random page of data and stores it. After reading
      the last possible page, executions ends and a collection with results is returned.
     */
    CompletableFuture<List<Page<Build>>> readingFuture = CompletableFuture.supplyAsync(() -> {
      List<Page<Build>> result = new ArrayList<>();
      while (true) {
        // query random page
        int page = random.nextInt(ITERATION_SIZE / PAGE_SIZE);
        Page<Build> builds = buildService.getBuilds(new PageRequest(page, PAGE_SIZE));
        result.add(builds);
        // if page has content - check iteration number and exit, it this is the last generation
        if (!builds.getContent().isEmpty()) {
          String type = builds.getContent().get(0).getBuildTypeId();
          String currentIteration = extractIteration(type);
          if (currentIteration.equals(String.valueOf(ITERATIONS_NUMBER-1))) {
            break;
          }
        } else {
          //if page is empty - test will fail in assert section
          break;
        }
      }
      return result;
    }, readingExecutor);

    writingFuture.join();
    readingFuture.join();

    try {
      List<Page<Build>> pages = readingFuture.get();
      for (Page<Build> page : pages) {
        /*
          Checks that page has PAGE_SIZE elements.
          This check guarantees that there are no 'dirty reads' where reading thread can read the
          data between delete and save operations.
         */
        Assert.assertTrue(page.getNumberOfElements() == PAGE_SIZE);

        Set<String> iterationNumbers = page.getContent().stream()
            .map(Build::getBuildTypeId)
            .map(this::extractIteration)
            .collect(Collectors.toSet());
        /*
          This check guarantees that there are no dirty reads, where reader can read object from
          different generations. In wrong setup, this can be an issue where BuildCollector firstly
          write the new data and then delete the old one.
         */
        Assert.assertTrue(iterationNumbers.size() == 1);
      }
    } catch (InterruptedException | ExecutionException e) {
      throw e;
    }
  }

  /**
   * Extracts generation from build type id.
   */
  private String extractIteration(String type) {
    return type.substring(0, type.indexOf("-"));
  }

  /**
   * Run a generation, the running increases iterationCounter.
   */
  private void runCollectionGeneration(Host host) {
    int iteration = iterationCounter.getAndIncrement();
    Collection<Build> builds = createBuilds(iteration, ITERATION_SIZE, host, instance);
    Mockito.when(teamcityClient.getAllRunningBuilds(Mockito.any())).thenReturn(builds);
    collector.collectBuilds();
  }

  /**
   * Creates test builds.
   *
   * @param iteration iteration number
   * @param count how much builds will be created
   * @param host host for builds
   * @param calendar startDate for builds
   */
  private Collection<Build> createBuilds(int iteration, int count, Host host, Calendar calendar) {
    List<Build> list = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      list.add(new Build(iteration * 100 + i, host, calendar, iteration + "-test-" + i));
    }
    return list;
  }
}
