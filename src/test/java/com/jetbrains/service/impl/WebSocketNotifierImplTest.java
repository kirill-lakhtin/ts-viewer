package com.jetbrains.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;

@RunWith(MockitoJUnitRunner.class)
public class WebSocketNotifierImplTest {

  private WebSocketNotifierImpl notifier;

  @Mock
  private SimpMessagingTemplate template;

  @Before
  public void setUp() throws Exception {
    notifier = new WebSocketNotifierImpl(template);
  }

  @Test
  public void sendHostNotification() throws Exception {
    notifier.sendHostNotification();

    Mockito.verify(template).convertAndSend("/topic/host", "refresh");
  }

  @Test
  public void sendBuildNotification() throws Exception {
    notifier.sendBuildNotification();

    Mockito.verify(template).convertAndSend("/topic/build", "refresh");
  }

}