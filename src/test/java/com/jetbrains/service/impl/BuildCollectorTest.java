package com.jetbrains.service.impl;

import com.jetbrains.agent.TeamcityClient;
import com.jetbrains.dao.BuildRepository;
import com.jetbrains.dao.HostRepository;
import com.jetbrains.model.Build;
import com.jetbrains.model.Host;
import com.jetbrains.service.WebSocketNotifier;
import jdk.nashorn.internal.runtime.arrays.IteratorAction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Tests for {@link BuildCollector}.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@RunWith(MockitoJUnitRunner.class)
public class BuildCollectorTest {

  private BuildCollector buildCollector;

  @Mock
  private JdbcTemplate template;

  @Mock
  private TeamcityClient teamcityClient;

  @Mock
  private HostRepository hostRepository;

  @Mock
  private BuildRepository buildRepository;

  @Mock
  private WebSocketNotifier webSocketNotifier;

  @Before
  public void setUp() throws Exception {
    TransactionTemplate transactionTemplate = new TransactionTemplate(Mockito.mock(
        PlatformTransactionManager.class));
    ExecutorService executorService = Executors.newSingleThreadExecutor();
    buildCollector = new BuildCollector(template, teamcityClient, hostRepository, executorService,
        transactionTemplate, buildRepository , webSocketNotifier,5000, 5000);
  }

  /**
   * Test checks that if db lock is acquired, app will load data from TC, find a diff between saved
   * data and old data and apply this diff to the DB. In this test there is no diff, so delete/save
   * methods shouldn't be called.
   */
  @Test
  public void collectBuildsLockAcquiredContentNotModified() throws Exception {
    Mockito.when(template.update(Mockito.anyString(), Mockito.any(Calendar.class), Mockito.any(Calendar.class))).thenReturn(1);
    Host host = new Host(1L, "www.test.com");
    Mockito.when(hostRepository.findAll()).thenReturn(Collections.singletonList(host));
    Build build = new Build(1L, host, Calendar.getInstance(), "test_build");
    List<Build> builds = Collections.singletonList(build);
    Mockito.when(teamcityClient.getAllRunningBuilds(host)).thenReturn(builds);
    Mockito.when(buildRepository.findAll()).thenReturn(builds);

    buildCollector.collectBuilds();

    Mockito.verify(buildRepository, Mockito.never()).delete(Mockito.any(Iterable.class));
    Mockito.verify(buildRepository, Mockito.never()).save(Mockito.any(Iterable.class));
  }


  /**
   * Test checks that if db lock is acquired, app will load data from TC, find a diff between saved
   * data and old data and apply this diff to the DB. In this test there is a diff, so delete/save
   * methods should be called and ws notification should be sent.
   */
  @Test
  public void collectBuildsLockAcquiredContentModified() throws Exception {
    Mockito.when(template.update(Mockito.anyString(), Mockito.any(Calendar.class), Mockito.any(Calendar.class))).thenReturn(1);
    Host host = new Host(1L, "www.test.com");
    Mockito.when(hostRepository.findAll()).thenReturn(Collections.singletonList(host));
    Calendar calendar = Calendar.getInstance();
    // new builds, collected from TC instances
    List<Build> newBuilds = Arrays.asList(
        createBuild(1L, host, calendar, "build_1"),
        createBuild(2L, host, calendar,  "build_2")
    );
    //old build, retrieved from the DB
    List<Build> oldBuilds = Arrays.asList(
        createBuild(2L, host, calendar, "build_2"),
        createBuild(3L, host, calendar, "build_3")
    );
    Mockito.when(teamcityClient.getAllRunningBuilds(host)).thenReturn(newBuilds);
    Mockito.when(buildRepository.findAll()).thenReturn(oldBuilds);

    buildCollector.collectBuilds();

    ArgumentCaptor<Iterable> deleteCaptor = ArgumentCaptor.forClass(Iterable.class);
    ArgumentCaptor<Iterable> saveCaptor = ArgumentCaptor.forClass(Iterable.class);
    Mockito.verify(buildRepository).delete(deleteCaptor.capture());
    Mockito.verify(buildRepository).save(saveCaptor.capture());
    Mockito.verify(webSocketNotifier).sendBuildNotification();
    Iterable<Build> deletedBuilds = deleteCaptor.getValue();
    Iterable<Build> savedBuilds = saveCaptor.getValue();
    //verify, that a build that presents in BD, but not in newly collected builds, is removed.
    Assert.assertEquals(3L, deletedBuilds.iterator().next().getId());
    //verify, that a build that is presented in collected builds, but not in the DB, is saved.
    Assert.assertEquals(1L, savedBuilds.iterator().next().getId());
  }

  /**
   * Test checks that if db lock is not acquired then no actions will be performed.
   */
  @Test
  public void collectBuildsLockNotAcquired() throws Exception {
    Mockito.when(template.update(Mockito.anyString(), Mockito.any(Calendar.class), Mockito.any(Calendar.class))).thenReturn(0);

    buildCollector.collectBuilds();
    Mockito.verify(hostRepository, Mockito.never()).findAll();
  }

  private Build createBuild(long id, Host host, Calendar calendar, String buildTypeId) {
    return new Build(id, host, calendar , buildTypeId);
  }
}