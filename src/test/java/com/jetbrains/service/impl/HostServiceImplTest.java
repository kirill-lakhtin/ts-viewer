package com.jetbrains.service.impl;

import com.jetbrains.dao.HostRepository;
import com.jetbrains.model.Host;
import com.jetbrains.service.HostService;
import com.jetbrains.service.exception.HostAlreadyExistsException;
import com.jetbrains.service.exception.HostDoesNotExistException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Tests for {@link HostServiceImpl}.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@RunWith(MockitoJUnitRunner.class)
public class HostServiceImplTest {

  private static final String TEST_URL = "http://www.test.com";
  private HostService instance;

  @Mock
  private HostRepository hostRepository;

  @Before
  public void setUp() throws Exception {
    instance = new HostServiceImpl(hostRepository);
  }

  /**
   * Checks that a new host is successfully added.
   */
  @Test
  public void addOK() throws Exception {
    Mockito.when(hostRepository.findByUrl(TEST_URL)).thenReturn(null);

    instance.add(TEST_URL);

    ArgumentCaptor<Host> argumentCaptor = ArgumentCaptor.forClass(Host.class);
    Mockito.verify(hostRepository).save(argumentCaptor.capture());
    Assert.assertEquals(TEST_URL, argumentCaptor.getValue().getUrl());
  }

  /**
   * Checks that the service doesn't allow hosts with identical url.
   */
  @Test(expected = HostAlreadyExistsException.class)
  public void addAlreadyExistedUrl() throws Exception {
    Mockito.when(hostRepository.findByUrl(TEST_URL)).thenReturn(new Host());

    instance.add(TEST_URL);
  }

  /**
   * Checks successful removing.
   */
  @Test
  public void deleteOK() throws Exception {
    Host host = new Host();
    Mockito.when(hostRepository.findOne(1L)).thenReturn(host);

    Host result = instance.delete(1L);

    Assert.assertEquals(host, result);
    Mockito.verify(hostRepository).delete(1L);
  }

  /**
   * Checks that exception will be thrown if the specified host doesn't exist.
   */
  @Test(expected = HostDoesNotExistException.class)
  public void deleteDoesNotExist() throws Exception {
    Mockito.when(hostRepository.findOne(1L)).thenReturn(null);

    instance.delete(1L);
  }

}