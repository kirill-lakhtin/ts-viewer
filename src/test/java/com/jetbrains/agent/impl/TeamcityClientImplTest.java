package com.jetbrains.agent.impl;

import com.jetbrains.agent.TeamcityClient;
import com.jetbrains.model.Build;
import com.jetbrains.model.Host;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;

/**
 * Tests for {@link TeamcityClientImpl}.
 *
 * @author Kirill Lakhtin (kirill.lakhtin@gmail.com)
 */
@RunWith(MockitoJUnitRunner.class)
public class TeamcityClientImplTest {

  private static final String RUNNING_BUILD_QUERY = "/guestAuth/app/rest/builds?locator"
      + "=state:running&fields=build(buildTypeId,startDate)";
  private static final String TEST_URL = "www.test.com";

  private TeamcityClient client;

  @Mock
  private RestTemplate restTemplate;

  @Before
  public void setUp() throws Exception {
    client = new TeamcityClientImpl(restTemplate);
  }

  /**
   * Checks that client returns an empty collection in the case of null response from restTemplate.
   */
  @Test
  public void getAllRunningBuildsEmptyResponse() throws Exception {
    String url = TEST_URL + RUNNING_BUILD_QUERY;
    ResponseEntity<TeamCityBuildDto> response = new ResponseEntity<>(new TeamCityBuildDto(null),
        HttpStatus.OK);
    Mockito.when(restTemplate.getForEntity(url, TeamCityBuildDto.class)).thenReturn(response);

    Collection<Build> builds = client.getAllRunningBuilds(new Host(1L, "www.test.com"));

    Assert.assertTrue(builds.isEmpty());
  }

  /**
   * Checks that client set host to the builds.
   */
  @Test
  public void getAllRunningBuildsFillHost() throws Exception {
    String url = TEST_URL + RUNNING_BUILD_QUERY;
    Build build = new Build(1, null, Calendar.getInstance(), "build");
    TeamCityBuildDto dto = new TeamCityBuildDto(Collections.singletonList(build));
    ResponseEntity<TeamCityBuildDto> response = new ResponseEntity<>(dto, HttpStatus.OK);
    Mockito.when(restTemplate.getForEntity(url, TeamCityBuildDto.class)).thenReturn(response);

    Collection<Build> builds = client.getAllRunningBuilds(new Host(1L, TEST_URL));

    Assert.assertEquals(TEST_URL, builds.iterator().next().getHost().getUrl());
  }

}